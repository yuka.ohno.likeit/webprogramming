package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class SeachServlet
 */
@WebServlet("/SeachServlet")
public class SeachServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SeachServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		���������h�~
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");
		
		if(userInfo == null) {
			response.sendRedirect("UserListServer");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		���������h�~
		request.setCharacterEncoding("UTF-8");
		
//		�p�����[�^�擾
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birth_date1 = request.getParameter("birth_date1");
		String birth_date2 = request.getParameter("birth_date2");
		
//		���X�g���
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findUserList(loginId, name, birth_date1, birth_date2);

//      ���N�G�X�g�X�R�[�v�Ƀ��[�U�ꗗ�����Z�b�g
		request.setAttribute("userList", userList);
		
//      �t�H���[�h
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/LoggedIn.jsp");
		dispatcher.forward(request, response);
	}

}