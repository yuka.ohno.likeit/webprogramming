package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;
import others.Code;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		���������h�~
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");
		
		
		
//		URL�p�����[�^����l���擾
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String id = request.getParameter("id");
//		������X�R�[�v�ɓ����
		request.setAttribute("loginId", loginId);
		request.setAttribute("password", password);
		request.setAttribute("id", id);
		
		if(userInfo == null) {
			response.sendRedirect("UserListServer");
		} else {
//		�Q�Ɖ�ʂɃt�H���[�h
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Update.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		���������h�~
		request.setCharacterEncoding("UTF-8");
		
//		�p�����[�^�擾
		String id = request.getParameter("id");
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		
		
		String Updatetedpassword = null;
		
//		パスワード暗号化
		Code code = new Code();
		password = code.MakeCode(password);
		
		
//		�p�X���[�h�m�F
		if(password1.equals(password2)) {
			Updatetedpassword = password1;
			Updatetedpassword = code.MakeCode(Updatetedpassword);
		} else {
			request.setAttribute("id", id);
			request.setAttribute("loginId", loginId);
			request.setAttribute("password", password);
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
//	    	�Ԉ���Ă���t�H���[�h
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Update.jsp");
			dispatcher.forward(request, response);
		}
		
//		�S�����͂���Ă邩�m�F
		if(
			name.equals("") ||
			birth_date.equals("")
			) 
		{
			request.setAttribute("id", id);
			request.setAttribute("loginId", loginId);
			request.setAttribute("password", password);
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
//	    	�����͂Ȃ�t�H���[�h
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Update.jsp");
			dispatcher.forward(request, response);
		}
		

//		�f�[�^�x�[�X�Ƀf�[�^�ǉ�
	    UserDao userDao = new UserDao();
	    int i = userDao.Update(id, password, name, birth_date, Updatetedpassword);
	   
//	    �Ԉ���Ă���t�H���[�h
	    if(i == 0) {
	    	request.setAttribute("id", id);
			request.setAttribute("loginId", loginId);
			request.setAttribute("password", password);
	    	request.setAttribute("errMsg", "入力された内容は正しくありません。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Update.jsp");
			dispatcher.forward(request, response);
	    }
	    
	 // ���[�U�ꗗ��ʂփ��_�C���N�g
	    response.sendRedirect("UserListServer");
	}

}
