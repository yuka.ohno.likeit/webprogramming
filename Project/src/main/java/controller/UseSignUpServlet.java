package controller;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;
import others.Code;
import others.Time;

/**
 * Servlet implementation class UseSignUpServlet
 */
@WebServlet("/UseSignUpServlet")
public class UseSignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UseSignUpServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		���������h�~
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");
		
		
		
		//���ݎ��Ԏ擾���郁�\�b�h�Ăяo��
		Time time = new Time();
		String date = time.Now();
		
		// ���N�G�X�g�X�R�[�v�Ɍ��ݎ��Ԃ��Z�b�g
		request.setAttribute("date", date);
		
		if(userInfo == null) {
			response.sendRedirect("UserListServer");
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SignUp.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		���������h�~
		request.setCharacterEncoding("UTF-8");
		
//		�p�����[�^�擾
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String create_date = request.getParameter("create_date");
		String update_date = request.getParameter("update_date");
		
		System.out.println(create_date);
		System.out.println(update_date);
		
		
		String password = null;
		
//		�S�����͂���Ă邩�m�F
		if(
			loginId.equals("") ||
			name.equals("") ||
			birth_date.equals("") ||
			password1.equals("") ||
			password2.equals("")
			) 
		{
			request.setAttribute("date", create_date);
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
//	    	�����͂Ȃ�t�H���[�h
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SignUp.jsp");
			dispatcher.forward(request, response);
		}
		
		
//		�p�X���[�h�m�F
		if(password1.equals(password2)) {
		  password = password1;
		  Code code = new Code();
		  password = code.MakeCode(password);
	    } else {
	    	request.setAttribute("date", create_date);
	    	request.setAttribute("errMsg", "入力された内容は正しくありません。");
//	    	�Ԉ���Ă���t�H���[�h
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SignUp.jsp");
			dispatcher.forward(request, response);
	    }
		
		
//		�f�[�^�x�[�X�Ƀf�[�^�ǉ�
	    UserDao userDao = new UserDao();
	    int i = userDao.InsertBySignUpInfo(loginId, name, birth_date, password, create_date, update_date);
	   
//	    �Ԉ���Ă���t�H���[�h
	    if(i == 0) {
	    	request.setAttribute("date", create_date);
	    	request.setAttribute("errMsg", "入力された内容は正しくありません。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SignUp.jsp");
			dispatcher.forward(request, response);
	    } else {
	    	
//		Dao����findByLoginInfo���\�b�h�Ăяo���ăp�����[�^�̒l������
		User user = userDao.findByLoginInfo(loginId, password);
			
//		�Z�b�V�����Ă�
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);
			
	    // ���[�U�ꗗ��ʂփ��_�C���N�g
	    response.sendRedirect("UserListServer");
	    }
		

}
}