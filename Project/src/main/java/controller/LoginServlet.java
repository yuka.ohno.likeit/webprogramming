package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;
import others.Code;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
//		���������h�~
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");
		
		if(userInfo != null) {
			response.sendRedirect("UserListServer");
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		���������h�~
		request.setCharacterEncoding("UTF-8");
		
//		�p�����[�^�擾
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		
		Code code = new Code();
		password = code.MakeCode(password);
		
		
//		Dao����findByLoginInfo���\�b�h�Ăяo���ăp�����[�^�̒l������
		UserDao userDao = new UserDao();
		User user = userDao.findByLoginInfo(loginId, password);
//		���O�C�������̂��Ǘ��҂Ȃ�1����ȊO��0��Jujgement�ɓ����
		int Jujgement = 0;
		
		/** �e�[�u���ɊY���̃f�[�^��������Ȃ������ꍇ(���O�C�����s��) **/
		if (user == null) {
		   // ���N�G�X�g�X�R�[�v�ɃG���[���b�Z�[�W���Z�b�g
	   	   request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります。");
		   // ���O�C��jsp�Ƀt�H���[�h
		   RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
		   dispatcher.forward(request, response);
		   return;
		}
		
//		���O�C�������̂��Ǘ��҂Ȃ�1����ȊO��0��Jujgement�ɓ����
		if(user.getId() == 1) {
			Jujgement = 1;
		}

//		�Z�b�V�����Ă�
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);
		session.setAttribute("Jujgement", Jujgement);
		
//		UserListServer�Ƀ��_�C���N�g
		response.sendRedirect("UserListServer");
		

	}

}