package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		���������h�~
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");
		
		
//		URL�p�����[�^����l���擾
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

//		sql�Ń��[�U��񌟍�
		UserDao dao = new UserDao();
		User user = dao.findByLoginInfo(loginId, password);
		
		
//		������X�R�[�v�ɓ����
		request.setAttribute("user", user);

		if(userInfo == null) {
			response.sendRedirect("UserListServer");
		} else {
//		�Q�Ɖ�ʂɃt�H���[�h
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDetail.jsp");
			dispatcher.forward(request, response);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}