package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mysql.jdbc.StringUtils;

import model.User;

public class UserDao {

	public User findByLoginInfo(String loginId, String password) {
		int idData = 0;
		Connection con = null;
		String loginIdData = null;
		String nameData = null;
		Date birthDateData = null;
		String passwordData = null;
		Date createDateData = null;
		Date updateDateData = null;
		
		
	    try {
	    // �f�[�^�x�[�X�֐ڑ�
	    con = DBManager.getConnection();
	    
//	    SQL��
	    String sql = "SELECT * FROM user WHERE login_id = ? AND password = ?";
	    
//	    �X�e�[�g�����g�ɓ����
	    PreparedStatement pstmt = con.prepareStatement(sql);
	    pstmt.setString(1, loginId);
	    pstmt.setString(2, password);
	    
//	    ����
	    ResultSet rs = pstmt.executeQuery();
	    
//	    �v�f����
	    if(!rs.next()) {
	    	return null;
	    }
	    
//	    �J����������l�Q�b�g����user�N���X�̃t�B�[���h�ɓ��ꂽ�̂�߂�
	    idData = rs.getInt("id");
	    loginIdData = rs.getString("login_id");
	    nameData = rs.getString("name");
	    birthDateData = rs.getDate("birth_date");
	    passwordData = rs.getString("password");
	    createDateData = rs.getTimestamp("create_date");    
	    updateDateData = rs.getTimestamp("update_date");
	    
	    
	   } catch (SQLException e) {
	        	e.printStackTrace();
	   } finally {
		 // �f�[�^�x�[�X�ؒf
        if (con != null) {
        	try {
              		con.close();
          	} catch (SQLException e) {
              		e.printStackTrace();
          	}
      	}
	   }
		return new User(idData, loginIdData, nameData, birthDateData, passwordData, createDateData, updateDateData);
	}
	
	public List<User> findAll(){
		Connection con = null;
        List<User> userList = new ArrayList<User>();
        
        try {
    	    // �f�[�^�x�[�X�֐ڑ�
    	    con = DBManager.getConnection();
    	    
//    	    SQL��
    	    String sql = "SELECT * FROM user WHERE id <> 1";
    	    
//    	    SELECT�������s�����ʕ[���擾
    	    Statement st = con.createStatement();
    	    ResultSet rs = st.executeQuery(sql);
    	    
    	    while(rs.next()) {
    	    	int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                Date createDate = rs.getTimestamp("create_date");
                Date updateDate = rs.getTimestamp("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
                
                userList.add(user);
    	    }
        } catch (SQLException e) {
        	e.printStackTrace();
   } finally {
	 // �f�[�^�x�[�X�ؒf
    if (con != null) {
    	try {
          		con.close();
      	} catch (SQLException e) {
          		e.printStackTrace();
      	}
  	}
   }
		return userList;
}
	
	public int InsertBySignUpInfo(String loginId, String name, String birth_date, String password,  String create_date, String update_date) {
		Connection con = null;
		int rs = 0;

	    try {
	    // �f�[�^�x�[�X�֐ڑ�
	    con = DBManager.getConnection();
	    
//	    SQL��
	    String sql = "INSERT INTO user(login_id, name, birth_date, password, create_date, update_date)" + "VALUE(?, ?, ?, ?, ?, ?)";
	    
//	    �X�e�[�g�����g�ɓ����
	    PreparedStatement pstmt = con.prepareStatement(sql);
	    pstmt.setString(1, loginId);
	    pstmt.setString(2, name);
	    pstmt.setString(3, birth_date);
	    pstmt.setString(4, password);
	    pstmt.setString(5, create_date);
	    pstmt.setString(6, update_date);
	    
//	    ����
	    rs = pstmt.executeUpdate();
	    
	    } catch (SQLException e) {
        	e.printStackTrace();
   } finally {
	 // �f�[�^�x�[�X�ؒf
    if (con != null) {
    	try {
          		con.close();
      	} catch (SQLException e) {
          		e.printStackTrace();
      	}
  	}
   }
		return rs;
	}
	
	public int Update(String id, String password, String name, String birth_date, String Updattedpassword) {
		Connection con = null;
		int rs = 0;

	    try {
	    // �f�[�^�x�[�X�֐ڑ�
	    con = DBManager.getConnection();
	    
//	    SQL��
	    String sql = "update user set password=?, name=?, birth_date=? where id=?";
	    
//	    �X�e�[�g�����g�ɓ����
	    PreparedStatement pstmt = con.prepareStatement(sql);
//	    �l�������ĂȂ��Ȃ猳�̃p�X���[�h������
          if(StringUtils.isNullOrEmpty(Updattedpassword)) {
        	  pstmt.setString(1, password);
          } else {
        	  pstmt.setString(1, Updattedpassword);
          }
	    pstmt.setString(2, name);
	    pstmt.setString(3, birth_date);
	    pstmt.setString(4, id);
	    
//	    ����
	    rs = pstmt.executeUpdate();
	    
	    } catch (SQLException e) {
        	e.printStackTrace();
   } finally {
	 // �f�[�^�x�[�X�ؒf
    if (con != null) {
    	try {
          		con.close();
      	} catch (SQLException e) {
          		e.printStackTrace();
      	}
  	}
   }
		return rs;
	}
	
	
	public int Delete(String id) {
		Connection con = null;
		int rs = 0;

	    try {
	    // �f�[�^�x�[�X�֐ڑ�
	    con = DBManager.getConnection();
	    
//	    SQL��
	    String sql = "DELETE FROM user WHERE id = ?";
	    
//	    �X�e�[�g�����g�ɓ����
	    PreparedStatement pstmt = con.prepareStatement(sql);
	    pstmt.setString(1, id);
	    
//	    ����
	    rs = pstmt.executeUpdate();
	    
	    } catch (SQLException e) {
        	e.printStackTrace();
   } finally {
	 // �f�[�^�x�[�X�ؒf
    if (con != null) {
    	try {
          		con.close();
      	} catch (SQLException e) {
          		e.printStackTrace();
      	}
  	}
   }
		return rs;
	}
	
	
	public List<User> findUserList(String loginid, String name, String Birth_Date1, String Birth_Date2) {
		Connection con = null;
		List<User> userList = new ArrayList<User>();

	    try {
	    // �f�[�^�x�[�X�֐ڑ�
	    con = DBManager.getConnection();
	    
//	    SQL��
	    String sql = "SELECT * FROM user WHERE id <> 1";
	    
	    if(!loginid.equals("")) {
	    	sql += " AND login_id ='" + loginid + "'";
	    } 
	    if(!name.equals("")) {
	    	sql += " AND name LIKE" + "'%" + name + "%'";	    	
	    }
	    if(!Birth_Date1.equals("") && !Birth_Date2.equals("")) {
	    	sql += " AND birth_date BETWEEN'" + Birth_Date1 + "' AND '" + Birth_Date2 + "'";
	    }
	    
//	    SELECT�������s�����ʕ[���擾
	    Statement st = con.createStatement();
	    ResultSet rs = st.executeQuery(sql);
	    
	    while(rs.next()) {
	    	int id1 = rs.getInt("id");
            String loginId1 = rs.getString("login_id");
            String name1 = rs.getString("name");
            Date birthDate1 = rs.getDate("birth_date");
            String password1 = rs.getString("password");
            Date createDate1 = rs.getTimestamp("create_date");
            Date updateDate1 = rs.getTimestamp("update_date");
            User user = new User(id1, loginId1, name1, birthDate1, password1, createDate1, updateDate1);
            
            userList.add(user);
	    }
	    

	    
	   } catch (SQLException e) {
	        	e.printStackTrace();
	   } finally {
		 // �f�[�^�x�[�X�ؒf
        if (con != null) {
        	try {
              		con.close();
          	} catch (SQLException e) {
              		e.printStackTrace();
          	}
      	}
	   }
		return userList;
	}
	
	
	

	
}
