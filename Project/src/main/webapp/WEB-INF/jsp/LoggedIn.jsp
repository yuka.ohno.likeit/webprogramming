<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:useBean id="date" class="java.util.Date"/>

<!DOCTYPE html>
<html>
<head>
<meta charset=”UTF-8">
<title>LoggedIn</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
</head>

<header style="background: #f0f0f0;">
<ul class="nav justify-content-end" style="text-align: center; height: 57px; letter-height: 57px;">
  <li class="nav-item">
    <p class="nav-link disabled" tabindex="-1" aria-disabled="true" style="margin-bottom: 0;">${userInfo.name}さん</p>
  </li>
  <li class="nav-item">
    <a class="nav-link active" href="LogoutServlet">ログアウト</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" href="UseSignUpServlet" style="font-size: 15px;">新規登録</a>
  </li>
  </ul>
  <hr style="margin: 0;">
</header>
<body>

<div class="blockquote text-center">
<h1 style="padding:30px 0px;">ユーザ一覧</h1>

<form action="SeachServlet" method="post" style="margin-top: 50px;">
  <div class="form-group row">
  <div class="col-sm-4" style="text-align: right; padding-right: 10px;">
    <label for="exampleInputEmail1">ログインID</label>
  </div>
  <div class="col-sm-8">
    <input type="text" name="loginId" class="form-control" id="exampleInputEmail1" style="width: 600px;">
  </div>
  </div>
  
  <div class="form-group row">
  <div class="col-sm-4" style="text-align: right; padding-right: 10px;">
    <label for="exampleInputPassword1">ユーザ名</label>
  </div>
  <div class="col-sm-8">
    <input type="text" name="name" class="form-control" id="exampleInputEmail1" style="width: 600px;">
  </div>
  </div>
  
  <div class="form-group row">
  <div class="col-sm-4" style="text-align: right; padding-right: 10px;">
    <label for="exampleInputPassword1">生年月日名</label>
  </div>
  <div class="col-sm-8" style="text-align: left;">
    <input type="date" name="birth_date1" max="9999-12-31">
    ~
    <input type="date" name="birth_date2" max="9999-12-31">
  </div>
  </div>

  <button type="submit" class="btn btn-primary" style="width: 200px;　margin-top: 30px;">検索</button>
</form>
</div>

<br>
<hr>
<br>

<table class="table" style="text-align: center;">
  <thead>
    <tr>
      <th scope="col">ログイン名</th>
      <th scope="col">ユーザ名</th>
      <th scope="col">生年月日</th>
      <th scope="col"></th>
    </tr>
  </thead>
  
  <tbody>
  
  

  <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td><fmt:formatDate value="${user.birthDate}" pattern="yyyy年MM月dd日" /></td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <td>
                     <c:if test="${Jujgement == 1}">
                       <a class="btn btn-primary" href="UserDetailServlet?loginId=${user.loginId}&password=${user.password}">詳細</a>
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}&loginId=${user.loginId}&password=${user.password}">更新</a>
                       <a class="btn btn-danger" href ="UserDeleteServlet?loginId=${user.loginId}&id=${user.id}">削除</a>
                     </c:if>
                     <c:if test="${Jujgement == 0}">
                       <a class="btn btn-primary" href="UserDetailServlet?loginId=${user.loginId}&password=${user.password}">詳細</a>
                        <c:if test="${user.id == userInfo.id}">
                        <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}&loginId=${user.loginId}&password=${user.password}">更新</a>
                        </c:if>
                     </c:if>
                     </td>
                   </tr>
                 </c:forEach>


  </tbody>
</table>


</body>
</html>