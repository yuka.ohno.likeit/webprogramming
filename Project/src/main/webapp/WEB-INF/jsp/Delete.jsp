<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset=”UTF-8">
<title>Delete</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
</head>

<header style="background: #f0f0f0;">
<ul class="nav justify-content-end" style="text-align: center; height: 57px; letter-height: 57px;">
  <li class="nav-item">
    <p class="nav-link disabled" tabindex="-1" aria-disabled="true" style="margin-bottom: 0;">${userInfo.name}さん</p>
  </li>
  <li class="nav-item">
    <a class="nav-link active" href="LogoutServlet">ログアウト</a>
  </li>
  </ul>
  <hr style="margin: 0;">
</header>
<body>

<div class="blockquote text-center">
<h1 style="padding:30px 0px;">ユーザ削除確認</h1>

<div style="margin-bottom: 50px;">
<p>ログインID：${loginId}</p>
<p>を本当に削除してよろしいでしょうか。</p>
</div>


<div class="row">
<div class="col-sm-6" style="text-align: right;">
<form action="UserListServer">
  <button type='submit' name='action' value='cancel'  class="btn btn-primary btn-lg" style="margin-right: 10px;">キャンセル</button>
</form>
</div>
<div class="col-sm-6" style="text-align: left;">
<form action="UserDeleteServlet?id=${id}" method="post">
  <button type='submit' name='action' value='del'  class="btn btn-primary btn-lg" style="width: 134px; margin-left: 10px;">OK</button>
</form>
</div>
</div>


</div>

</body>
</html>