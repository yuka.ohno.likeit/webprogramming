<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset=”UTF-8">
<title>Login</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>

<div class="blockquote text-center">
<h1 style="padding:30px 0px;">ログイン</h1>

<form action="LoginServlet" method="post" style="margin-top: 50px;">
  <div class="form-group row">
  <div class="col-sm-4" style="text-align: right; padding-right: 10px;">
    <label for="exampleInputEmail1">ログインID</label>
  </div>
  <div class="col-sm-8">
    <input type="text" name="loginId" class="form-control" id="exampleInputEmail1" style="width: 500px;">
  </div>
  </div>
  
  <div class="form-group row">
  <div class="col-sm-4" style="text-align: right; padding-right: 10px;">
    <label for="exampleInputPassword1">パスワード</label>
  </div>
  <div class="col-sm-8">
    <input type="password" name="password" class="form-control" id="exampleInputEmail1" style="width: 500px;">
  </div>
  </div>
	
	<p>	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if></p>
    
  <button type="submit" class="btn btn-primary" style="width: 200px; margin-top: 30px;">ログイン</button>
</form>
</div>

</body>
</html>