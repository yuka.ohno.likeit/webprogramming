<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset=”UTF-8">
<title>SignUp</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
</head>

<header style="background: #f0f0f0;">
<ul class="nav justify-content-end" style="text-align: center; height: 57px; letter-height: 57px;">
  <li class="nav-item">
    <p class="nav-link disabled" tabindex="-1" aria-disabled="true" style="margin-bottom: 0;">${userInfo.name}さん</p>
  </li>
  <li class="nav-item">
    <a class="nav-link active" href="LogoutServlet">ログアウト</a>
  </li>
  </ul>
  <hr style="margin: 0;">
</header>
<body>

<div class="blockquote text-center">
<h1 style="padding:30px 0px;">ユーザ新規登録</h1>

<form action="UseSignUpServlet" method="post" style="margin-top: 50px;">
  <div class="form-group row">
  <div class="col-sm-4" style="text-align: right; padding-right: 10px;">
    <label for="exampleInputEmail1">ログインID</label>
  </div>
  <div class="col-sm-8">
    <input type="text" name="loginId" class="form-control" id="exampleInputEmail1" style="width: 600px;">
  </div>
  </div>
  
  <div class="form-group row">
  <div class="col-sm-4" style="text-align: right; padding-right: 10px;">
    <label for="exampleInputPassword1">パスワード</label>
  </div>
  <div class="col-sm-8">
    <input type="password" name="password1" class="form-control" id="exampleInputEmail1" style="width: 600px;">
  </div>
  </div>
  
    <div class="form-group row">
  <div class="col-sm-4" style="text-align: right; padding-right: 10px;">
    <label for="exampleInputPassword1">パスワード(確認)</label>
  </div>
  <div class="col-sm-8">
    <input type="password" name="password2" class="form-control" id="exampleInputEmail1" style="width: 600px;">
  </div>
  </div>
  
  <div class="form-group row">
  <div class="col-sm-4" style="text-align: right; padding-right: 10px;">
    <label for="exampleInputPassword1">ユーザ名</label>
  </div>
  <div class="col-sm-8">
    <input type="text" name="name" class="form-control" id="exampleInputEmail1" style="width: 600px;">
  </div>
  </div>
  
  <div class="form-group row">
  <div class="col-sm-4" style="text-align: right; padding-right: 10px;">
    <label for="exampleInputPassword1">生年月日</label>
  </div>
  <div class="col-sm-8">
    <input type="date" name="birth_date" class="form-control" id="exampleInputEmail1" style="width: 600px;">
  </div>
  </div>
  
<input type="hidden" name="create_date" value="${date}">
<input type="hidden" name="update_date" value="${date}">

<p>	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if></p>
  
  <button type="submit" class="btn btn-primary" style="width: 200px; margin-bottom: 30px;">登録</button>
</form>
</div>

<div class="col-sm-4" style="text-align: right;">
<ul style="list-style: none;">
<li class="nav-item">
    <a class="nav-link active" href="UserListServer">戻る</a>
  </li>
</ul>
</div>

</body>
</html>